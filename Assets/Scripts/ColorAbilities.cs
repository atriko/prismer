﻿using UniRx;
using UnityEngine;

public class ColorAbilities : MonoBehaviour
{
    public ReactiveProperty<Colors> primaryColor = new ReactiveProperty<Colors>(Colors.Red);

    [SerializeField] private GameObject speedParticle;
    private PlayerControls playerControls;

    public enum Colors
    {
        Red = 0,
        Green = 1,
        Blue = 2
    }
    private void Start()
    {
        playerControls = FindObjectOfType<PlayerControls>();
        primaryColor.ObserveEveryValueChanged(property => property.Value).Subscribe(delegate(Colors color)
        {
            if (color == Colors.Blue)
            {
                playerControls.canAttack = false;
                playerControls.canJump = false;
                speedParticle.SetActive(true);
                Debug.Log("Blue");
            }
            else if (color == Colors.Green)
            {
                speedParticle.SetActive(false);
                playerControls.canAttack = false;
                playerControls.canJump = true;
                Debug.Log("Green");
            }
            else if (color == Colors.Red)
            {
                speedParticle.SetActive(false);
                playerControls.canJump = false;
                playerControls.canAttack = true;
                Debug.Log("Red");
            }
        });
    }
    public void ChangeToNextColor()
    {
        int nextColorIndex = (int)primaryColor.Value + 1;
        if (nextColorIndex < 3)
        {
            primaryColor.Value = (Colors)nextColorIndex;
        }
        else
        {
            primaryColor.Value = 0;
        }
    }
    public void ChangeToPreviousColor()
    {
        int previousColorIndex = (int)primaryColor.Value - 1;
        if (previousColorIndex >= 0)
        {
            primaryColor.Value = (Colors)previousColorIndex;
        }
        else
        {
            primaryColor.Value = (Colors) 2;
        }
    }
    public Colors GetCurrentColor()
    {
        return primaryColor.Value;
    }
}
