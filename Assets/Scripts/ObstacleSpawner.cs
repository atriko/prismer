﻿using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] obstacles = default;

    private float spawnRate = 1.5f;
    private float nextSpawn = 0;

    void Update()
    {
        if (Time.time > nextSpawn) 
        {
            int randomObstacle = UnityEngine.Random.Range(0, obstacles.Length);
            Instantiate(obstacles[randomObstacle], new Vector3(transform.position.x,obstacles[randomObstacle].transform.position.y,transform.position.z), Quaternion.identity);
            nextSpawn = Time.time + spawnRate;
        }
    }
}
