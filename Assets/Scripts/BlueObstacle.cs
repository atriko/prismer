﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueObstacle : Obstacle
{
    
    void Update()
    {
        rb.velocity = new Vector3(0, 0, -speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameObject.SetActive(false);
        }
    }
}
