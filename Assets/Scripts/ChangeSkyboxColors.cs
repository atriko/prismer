﻿using DG.Tweening;
using UniRx;
using UnityEngine;
using Colors = ColorAbilities.Colors;

public class ChangeSkyboxColors : MonoBehaviour
{
    [SerializeField] private Color redSkybox;
    [SerializeField] private Color blueSkybox;
    [SerializeField] private Color greenSkybox;

    private ReactiveProperty<Colors> primaryColor;

    private Color currentColor;
    private Color targetColor;
    private bool isLerping;
    void Start()
    {
        currentColor = redSkybox;
        targetColor = currentColor;
        RenderSettings.skybox.SetColor("_Tint", currentColor);
        primaryColor = GetComponent<ColorAbilities>().primaryColor;
        primaryColor.ObserveEveryValueChanged(property => property.Value).Subscribe(delegate(Colors color)
        {
            if (color == Colors.Red)
            {
                targetColor = redSkybox;

            }
            else if (color == Colors.Blue)
            {
                targetColor = blueSkybox;

            }
            else if (color == Colors.Green)
            {
                targetColor = greenSkybox;

            }
            isLerping = true;
        });
        Observable.EveryUpdate().Where(l => isLerping).Subscribe(delegate
        {
            isLerping = false;
            DOTween.To(() => RenderSettings.skybox.GetColor("_Tint"),
                value => RenderSettings.skybox.SetColor("_Tint",value),
                targetColor,0.2f);
        });
    }
}
