﻿using System;
using DG.Tweening;
using UniRx;
using UnityEngine;
using Colors = ColorAbilities.Colors;

public class PlayerControls : MonoBehaviour
{
    public bool canJump;
    public bool canAttack;

    private ColorAbilities colorAbilities;
    private float rotationSpeed = 0.2f;
    private bool isGrounded = true;
    private Vector3 currentRotation = new Vector3(0, 0, 0);

    private readonly Vector3 turnRotation = new Vector3(0, 120, 0);

    private void Start()
    {
        colorAbilities = GetComponent<ColorAbilities>();
        Observable.EveryUpdate().Subscribe(delegate
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                colorAbilities.ChangeToNextColor();
                RotateRight();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                colorAbilities.ChangeToPreviousColor();
                RotateLeft();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                if (canJump)
                {
                    Jump();
                }
                else if (canAttack)
                {
                    //Attack
                }
            }
        });
    }

    private void Jump()
    {
        if (!isGrounded) return;
        isGrounded = false;
        transform.DOJump(transform.position, 20f, 0, 0.5f).onComplete = () => isGrounded = true;

        // TODO: Flip on Jump
//        Sequence s = DOTween.Sequence();
//        s.Append(transform.DOJump(transform.position, 30f, 0, 0.6f));
//        s.Join(playerRoot.DORotate(new Vector3(360, 0, 0), 0.5f, RotateMode.WorldAxisAdd));
//        s.onComplete = () => isGrounded = true;
//        s.Play();
    }

    private void RotateLeft()
    {
        currentRotation += turnRotation;
        transform.DORotate(currentRotation, rotationSpeed);
    }

    private void RotateRight()
    {
        currentRotation -= turnRotation;
        if (Math.Abs(currentRotation.y + 360f) < 20f) // Needs refactor when rotation degree exceeds -360 behaviour changes. Not reproduced in positive degrees Possible bug in DoTween
        {
            currentRotation.y = 0;
        }
        transform.DORotate(currentRotation, rotationSpeed);
    }
}