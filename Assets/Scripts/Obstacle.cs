﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    protected Rigidbody rb;
    protected static float speed = 350f;
    private float nextSpeedUp;
    private float speedUpRate = 0.4f;
    private static bool gainingSpeed;

    // Start is called before the first frame update
    void Start()
    {
        if (gainingSpeed)
        {
            if (Time.time > nextSpeedUp)
            {
                speed += 100f;
                nextSpeedUp = Time.time + speedUpRate;
            }
        }
        else
        {
            speed = 350f;
        }
        rb = GetComponent<Rigidbody>();
    }
    public static void SpeedUp()
    {
        gainingSpeed = true;
    }

    internal static void SpeedDown()
    {
        gainingSpeed = false;
    }
}
